package br.edu.up.dominio;

import java.util.ArrayList;
import java.util.List;

public class Disciplina {
	
	private static final double MEDIA = 6;
	
	private String nomeDoAluno;
	private String nomeDaDisciplina;
	
	private List<Prova> provas = new ArrayList<>();
	
	
	public double getNotaFinal() {
		//TODO: l�gica de c�lculo da nota final;
		return 0;
	}
	
	public boolean getAprovado() {
		//TODO: l�gica para verificar se est� aprovado;
		return true;
	}

	public String getNomeDoAluno() {
		return nomeDoAluno;
	}

	public void setNomeDoAluno(String nomeDoAluno) {
		this.nomeDoAluno = nomeDoAluno;
	}

	public String getNomeDaDisciplina() {
		return nomeDaDisciplina;
	}

	public void setNomeDaDisciplina(String nomeDaDisciplina) {
		this.nomeDaDisciplina = nomeDaDisciplina;
	}

	public List<Prova> getProvas() {
		return provas;
	}

	public void setProvas(List<Prova> provas) {
		this.provas = provas;
	}
}
